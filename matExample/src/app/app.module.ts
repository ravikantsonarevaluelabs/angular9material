import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule, routingComponents } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialConfigModule } from './material-config/material-config.module';
import { EmployeeRegistrationComponent } from './employee-registration/employee-registration.component';
import { EmployeeListComponent } from './employee-list/employee-list.component';
import { DepartmentListComponent } from './department-list/department-list.component';
import { DepartmentListService } from './department-list/department-list.service';
import { EmployeeListService } from './employee-list/employee-list.service';

@NgModule({
  declarations: [
    AppComponent,
    routingComponents,
    EmployeeRegistrationComponent,
    EmployeeListComponent,
    DepartmentListComponent
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialConfigModule,
    HttpClientModule
  ],
  providers: [DepartmentListService, EmployeeListService],
  bootstrap: [AppComponent]
})
export class AppModule { }
