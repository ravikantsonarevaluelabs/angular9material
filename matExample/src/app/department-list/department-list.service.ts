import { Injectable } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
// import { url } from 'inspector';
import { Idepartment } from './department';
import { Observable } from 'rxjs';
export interface PeriodicElement {
  name: string;
  position: number;
  incharge:string;
}

@Injectable({
  providedIn: 'root'
})
export class DepartmentListService {
  private _url: string = "/assets/data/department-list.json";
  constructor(private http: HttpClient) { }
  getEmplooyes(): Observable<Idepartment[]> {
    return this.http.get<Idepartment[]>(this._url);
  }
}
