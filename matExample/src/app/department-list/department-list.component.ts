import { Component, OnInit } from '@angular/core';
import { DepartmentListService } from './department-list.service';

@Component({
  selector: 'app-department-list',
  templateUrl: './department-list.component.html',
  styleUrls: ['./department-list.component.css']
})
export class DepartmentListComponent implements OnInit {
  public dataSource = [];
  constructor(private _departmentService: DepartmentListService) { }
  displayedColumns: string[] = ['position', 'name', 'incharge'];

  ngOnInit(): void {
    this._departmentService.getEmplooyes()
      .subscribe(data => this.dataSource = data);
  }

}
