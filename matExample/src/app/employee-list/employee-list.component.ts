import { Component, OnInit } from '@angular/core';
import { EmployeeListService } from './employee-list.service';



@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})

export class EmployeeListComponent implements OnInit {

  constructor(private __employeeService: EmployeeListService) { }
  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];
  dataSource = this.__employeeService.getEmployeeService();
  ngOnInit(): void {
  }

}
